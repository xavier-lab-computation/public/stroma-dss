# Colon Stroma scRNAseq

**Stromal heterogeneity and molecular circuits driving extracellular matrix deposition and remodeling during inflammation and fibrosis.**

**Data collection**
We implemented a murine model of intestinal inflammation based on oral administration of multiple cycles of low dose dextran sulfate sodium (DSS) to induce epithelial injury and ECM deposition. Mice were subjected to 3 repetitive cycles of DSS displayed progressive accumulation of immune cell infiltrates associated with excessive deposition of collagen fibers. Lamina propria cells from water-fed and DSS-fed mice were isolated using enzymatic digestion,  and enriched for stromal cells by FACS using antibodies excluding hematopoietic cells (CD45), epithelial cells (EpCAM), and erythrocytes. Prepared single cell suspensions were then profiled using the 10x Chromium V2 droplet-based single cell RNA sequencing platform. Tissues were collected from proximal and distal colon. A total of 3 female mice (12 weeks old) were processed for each treatment group and analyzed by single-cell RNA-seq. 

**Data processing**
Demultiplex and basecall to fastq with cellranger mkfastq pipeline made available by CellRanger v2.2 software (10x Genomics).
FASTQ reads were aligned to the mm10 mouse transcriptome, to extract cell barcodes and UMI barcodes using the cellranger count function. A digital gene expression (DGE) matrix for each sample was processed further.
Cells that satisfied any one of the following criteria were removed: (1) < 300 detected genes; 2) outlier number of Unique Molecular Identifiers (UMIs); 3) outlier proportion of mitochondrial gene expression were excluded. Outlier cutoffs for each batch of samples were determined empirically based on the distribution of UMI and proportion of mitochondrial gene expression per cell; or 4) doublets identified by the python package Scrublet
