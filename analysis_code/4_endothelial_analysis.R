###### Endothelial cells - Reclustering #######

endothel.anchors <- subset(stroma.anchors, idents =c("BEC 1", "BEC 2", "LEC"), invert=F)
DefaultAssay(endothel.anchors) = "integrated"
endothel.anchors <- RunPCA(endothel.anchors, 
                           do.print = TRUE, 
                           seed.use = 123, 
                           npcs = 100,
                           ndims.print = 1,
                           verbose = T)
#Check Elbow
ElbowPlot(object = endothel.anchors, ndims = 100)

#Clustering
k = 50
sig.pcs = 50
res = 1
endothel.anchors =  FindNeighbors(endothel.anchors, k.param = k, dims = 1:sig.pcs, compute.SNN = T, reduction="pca", verbose = T)
endothel.anchors =  FindClusters(endothel.anchors, dims.use = 1:sig.pcs, algorithm = 1, save.SNN = T, resolution = res, reduction = "pca",verbose = T)
endothel.anchors =  RunUMAP(object = endothel.anchors,assay = "integrated",seed.use = 123,dims = 1:sig.pcs)

pBEC = DimPlot(object = endothel.anchors, label = T, pt.size = 0.1, repel = F, reduction = "umap")
pBEC


###### Cluster Annotation and  UMAP - Endothelium ########

endothel.anchors <- RenameIdents(endothel.anchors, 
                                 `0` = "Capillary 1", 
                                 `1` = "Capillary 2",
                                 `2` = "Arteriole", 
                                 `3` = "LEC", 
                                 `4` = "Venule 1", 
                                 `5` = "Vein", 
                                 `6` = "Artery", 
                                 `7` = "Venule 2")
levels(endothel.anchors) <- levels(endothel.anchors)[c(7,3,1,2,5,8,6,4)]
###### UMAP - Endothelial cells ######

#Annotated UMAP plot
seu = endothel.anchors
seu@meta.data$ColVec = NA
seu@meta.data$ColVec[seu@meta.data$ClusterLabel %in% "Artery"] = "#c01714"
seu@meta.data$ColVec[seu@meta.data$ClusterLabel %in% "Arteriole"] = "#d25c5a"
seu@meta.data$ColVec[seu@meta.data$ClusterLabel %in% "Capillary 1"] = "#e5a2a1"
seu@meta.data$ColVec[seu@meta.data$ClusterLabel %in% "Capillary 2"] = "#f2d0d0"
seu@meta.data$ColVec[seu@meta.data$ClusterLabel %in% "Venule 1"] = "#a2a1e5"
seu@meta.data$ColVec[seu@meta.data$ClusterLabel %in% "Venule 2"] = "#7372d9"
seu@meta.data$ColVec[seu@meta.data$ClusterLabel %in% "Vein"] = "#2e2bc6"
seu@meta.data$ColVec[seu@meta.data$ClusterLabel %in% "LEC"] = "#8219e4"
seu@meta.data$ColVec <- toupper(seu@meta.data$ColVec)

#Arrange col vectors according to name sort
clusterOrder <- as.character(sort(unique(seu@active.ident)))
clusterOrder <- unique(seu@meta.data[,c(17,19)])
clusterOrder <- clusterOrder[order(clusterOrder$ClusterLabel), ]
colr.vec <- clusterOrder$ColVec

pAnnot <- DimPlot(object = seu, label=T, pt.size = 0.4, repel = T, group.by = "ClusterLabel", cols = colr.vec)
pAnnot <- pAnnot + theme(axis.text =element_text(family="Helvetica", face="plain", size=12), 
                         axis.title =element_text(family="Helvetica", face="bold", size=12)) 
pAnnot <- pAnnot + ggtitle(" ") + theme(legend.position="right") + xlab("UMAP 1") + ylab("UMAP 2")
pAnnot 


#By Condition
pAnnot1 <- DimPlot(object = seu, label=T, pt.size = 0.4, repel = T, group.by = "ClusterLabel", cols = colr.vec, split.by = "Treatment")
pAnnot1 <- pAnnot1 + theme(axis.text =element_text(family="Helvetica", face="plain", size=12), 
                           axis.title =element_text(family="Helvetica", face="bold", size=12)) 
pAnnot1 <- pAnnot1 + ggtitle(" ") + theme(legend.position="right") + xlab("UMAP 1") + ylab("UMAP 2")
pAnnot1 

###### Dotplot of Canonical markers #######
venous.markers <- c("Plvap", "Ephb4","Nr2f2",  "Aplnr", "Nrp2","Aqp1" )
arterial.markers <- c("Stmn2",  "Hey1","Jag1", "Gja4", "Gja5",  "Notch3", "Notch4","Efnb2", "Lyve1")
g1 <- c(venous.markers, arterial.markers)
p <- DotPlot(endothel.anchors,assay="SCT", rev(g1), cols =c("midnightblue", "darkorange1")) + RotatedAxis()
p + scale_y_discrete(limits = rev(unique(sort(unique(endothel.anchors@active.ident)))))
